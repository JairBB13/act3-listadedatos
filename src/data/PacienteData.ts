import type { IPaciente } from "@/interface/IPaciente" 

const PacienteData: IPaciente[] = [
    {
        nombreDueño:  'Edgar',
        numeroDueño: '9987263717',
        nombreMascota: 'Odie',
        alta: '2024-02-09',
        sintomas: 'No quiere comer'
    },
    {
        nombreDueño: "María",
        numeroDueño: '9876543210',
        nombreMascota: "Pelusa",
        alta: "2024-02-10",
        sintomas: "Diarrea persistente"
    },
    {
        nombreDueño: "Juan",
        numeroDueño: "8765432109",
        nombreMascota: "Max",
        alta: "2024-02-10",
        sintomas: "Cojera en la pata trasera"
    },
    {
        nombreDueño: "Laura",
        numeroDueño: "7654321098",
        nombreMascota: "Luna",
        alta: "2024-02-09",
        sintomas: "Vómitos frecuentes"
    },
    {
        nombreDueño: "Pedro",
        numeroDueño: "6543210987",
        nombreMascota: "Rex",
        alta: "2024-02-08",
        sintomas: "Pérdida de apetito"
    },
    {
        nombreDueño: "Ana",
        numeroDueño: "5432109876",
        nombreMascota: "Bobby",
        alta: "2024-02-10",
        sintomas: "Secreción ocular"
    },
    {
        nombreDueño: "Carlos",
        numeroDueño: "4321098765",
        nombreMascota: "Toby",
        alta: "2024-02-11",
        sintomas: "Picazón y enrojecimiento en la piel"
    },
    {
        nombreDueño: "Patricia",
        numeroDueño: "3210987654",
        nombreMascota: "Lucky",
        alta: "2024-02-10",
        sintomas: "Tos persistente"
    },
    {
        nombreDueño: "Roberto",
        numeroDueño: "2109876543",
        nombreMascota: "Simba",
        alta: "2024-02-09",
        sintomas: "Letargo y falta de energía"
    },
    {
        nombreDueño: "Isabel",
        numeroDueño: "1098765432",
        nombreMascota: "Coco",
        alta: "2024-02-11",
        sintomas: "Dificultad para respirar"
    },
    {
        nombreDueño: "Luis",
        numeroDueño: "0987654321",
        nombreMascota: "Lola",
        alta: "2024-02-10",
        sintomas: "Pérdida de peso inexplicada"
    }
    
    
]

export default PacienteData