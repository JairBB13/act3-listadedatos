export interface IPaciente {
    nombreDueño: string;
    numeroDueño: string;
    nombreMascota: string;
    alta: string;
    sintomas: string;
}